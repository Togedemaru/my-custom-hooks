import { useEffect, useReducer } from "react";
import { todoReducer } from "./todoReducer";



const init = () => { 
    if(localStorage['todos']){
        return JSON.parse( localStorage.getItem('todos') || [] );
    }
    return [];
}



export const useTodo = () => {

    const [todos, dispatch] = useReducer( todoReducer , [], init );

    const todosCount = todos.length;

    const pendingTodosCount = todos.filter( todo => todo.done === false).length;

    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify( todos ) ||  []);
    }, [todos])
    
    const handleNewTodo =  ( todo ) => {
        const action = {
            type: '[TODO] Add Todo',
            payload: todo
        }
        dispatch( action );
    };

    const handleDeleteTodo =  ( id ) => {
        const action = {
            type: '[TODO] Delete Todo',
            payload: id
        }
        dispatch( action );
    };


    const handleUpdateTodo =  ( id ) => {
        const action = {
            type: '[TODO] Update Todo',
            payload: id
        }
        dispatch( action );
    };


    return {
        todos,
        todosCount,
        pendingTodosCount,
        handleNewTodo,
        handleDeleteTodo,
        handleUpdateTodo,
    }
}
