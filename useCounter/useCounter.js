import React, { useState } from 'react';

export const useCounter = ( initialValue = 1 ) => {
  
    const [mycounter, setMycounter] = useState( initialValue )
    
    const increment = (value = 1)=>{
        setMycounter( (current) => current + value );
    };

    const decrement = ( value = 1 )=>{
        if( mycounter < value )return;
        setMycounter( (current) => current - value );
    };

    const reset = ()=>{
        setMycounter( initialValue );
    };
  
    return {
        mycounter,
        increment,
        decrement,
        reset,
    }
}
