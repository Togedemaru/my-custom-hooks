import { useEffect, useState } from "react";


export const useFetch = (url) => {

    const [pokes, setPokes] = useState({
        data: null,
        isLoading: true,
        error: null,
    });

    const useFetch = async () => { 
        console.log(url);
        setPokes({
            ...pokes,
            isLoading: true,
        });
        const resp = await fetch(url);
        const data = await resp.json();

        setPokes({
            data,
            isLoading: false,
            error: false,
        });

    };

    useEffect(() => {
        useFetch();
    }, [url]);
    



    return {
        data: pokes.data,
        isLoading: pokes.isLoading,
        error: pokes.error,
    };
}
