import { useState } from "react";

export const useForm = ( initStateForm = {} ) => {

    const [formState, setFormState] = useState( initStateForm );

    const handleInputChange = ({target}) => {
        const {name, value} = target;
        setFormState({
            ...formState,
            [ name ]: value
        });
    };

    const handleResetForm = () => { 
        setFormState( initStateForm );
    };


  return {
    ...formState,
    formState,
    handleInputChange,
    handleResetForm,
  }
}
